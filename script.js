
var list_grades = [];
var log = document.querySelector(".log");
var final = document.querySelector("#final");
var flag = 0;
function isNumber(str){
    return str != NaN;
}

function addGrade(){
    if(flag){
        flag = 0;
        list_grades=[];
        log.innerHTML = "";
        final.style.visibility="hidden";
    }
    let input = document.querySelector("#grade");
    if(input.value === ""){
        //Verifica se o input está vazio
        alert("Por favor, insira uma nota.");
    }
    else
    {
        let str_grade = input.value.replace(',', '.');
        let grade = parseFloat(str_grade);
        console.log(grade);
        if(isNumber(grade) && (grade>=0 && grade<=10)){
                list_grades.push(grade);
                addGradesToLog(grade, list_grades.length);
        }else{
            alert("A nota digitada é inválida, por favor, insira uma nova nota.");
        }
        input.value = '';

        
    } 
}

function addGradesToLog(grade, length){
    

    
    let message = document.createElement("p");
    message.innerText="A nota " + length + " foi " + grade.toFixed(2);

    log.append(message);
    
}

function calculateFinalGrade(){
    let sum = 0;
    list_grades.forEach((g) => sum+=g );
    let finalGrade = sum/list_grades.length;
    
    
    final.innerText=finalGrade.toFixed(2);
    final.style.visibility = 'visible';
    flag = 1;
}


let btnAdd = document.querySelector("#add");
btnAdd.addEventListener("click", addGrade);

let btnCalcGrade = document.querySelector("#calc");
btnCalcGrade.addEventListener("click", calculateFinalGrade);